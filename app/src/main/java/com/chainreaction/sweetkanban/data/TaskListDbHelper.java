package com.chainreaction.sweetkanban.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.chainreaction.sweetkanban.data.KanbanContract.TaskListEntry;

public class TaskListDbHelper extends SQLiteOpenHelper {
    private static TaskListDbHelper sInstance;
    private static final String DATABASE_NAME = "tasklist.db";

    // If change the database schema, increment the database version
    private static final int DATABASE_VERSION = 4;

    private TaskListDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized TaskListDbHelper getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        if (sInstance == null) {
            sInstance = new TaskListDbHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // table to hold kanban list data
        final String SQL_CREATE_TASK_TABLE = "CREATE TABLE " + TaskListEntry.TABLE_NAME + " (" +
                TaskListEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                TaskListEntry.COLUMN_TASK_NAME + " TEXT NOT NULL, " +
                TaskListEntry.COLUMN_TASK_ASSIGN_TO + " TEXT NOT NULL, " +
                TaskListEntry.COLUMN_TASK_COLOR + " TEXT NOT NULL, " +
                TaskListEntry.COLUMN_TASK_DEADLINE + " DATE NOT NULL, " +
                TaskListEntry.COLUMN_TASK_POSITION + " INTEGER NOT NULL," +
                TaskListEntry.COLUMN_TASK_PRIORITY + " INTEGER NOT NULL," +
                TaskListEntry.COLUMN_ID_CATEGORY + " INTEGER NOT NULL" +
                "); ";

        sqLiteDatabase.execSQL(SQL_CREATE_TASK_TABLE); // pass the string query
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // For now simply drop the table and create a new one. This means if you change the
        // DATABASE_VERSION the table will be dropped.
        // TODO In a production app, this method might be modified to ALTER the table
        // instead of dropping it, so that existing data is not deleted.
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TaskListEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}