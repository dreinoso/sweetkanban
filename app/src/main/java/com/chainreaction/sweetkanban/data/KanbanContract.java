package com.chainreaction.sweetkanban.data;

import android.provider.BaseColumns;

/**
 * Created by denis on 31/05/17.
 */

public class KanbanContract {

    public static final class KanbanListEntry implements BaseColumns {
        public static final String TABLE_NAME = "kanbanList";
        public static final String COLUMN_KANBAN_NAME = "kanbanName";
        public static final String COLUMN_KANBAN_COLOR = "kanbanColor";
        public static final String COLUMN_KANBAN_POSITION = "kanbanPosition";
        public static final String COLUMN_KANBAN_START_CATEGORY_ID = "startCategoryID";
    }

    public static final class CategoryListEntry implements BaseColumns {
        public static final String TABLE_NAME = "categoryList";
        public static final String COLUMN_CATEGORY_NAME = "categoryName";
        public static final String COLUMN_CATEGORY_COLOR = "categoryColor";
        public static final String COLUMN_CATEGORY_POSITION = "categoryPosition";
        public static final String COLUMN_CATEGORY_TASK_COUNT = "taskCount";
        public static final String COLUMN_CATEGORY_KANBAN_ID = "idKanban";
    }

    public static final class TaskListEntry implements BaseColumns {
        public static final String TABLE_NAME = "taskList";
        public static final String COLUMN_TASK_NAME = "taskName";
        public static final String COLUMN_TASK_COLOR = "taskColor";
        public static final String COLUMN_TASK_POSITION = "taskPosition";
        public static final String COLUMN_TASK_DEADLINE = "deadline";
        public static final String COLUMN_TASK_ASSIGN_TO = "assignTo";
        public static final String COLUMN_TASK_PRIORITY = "priority";
        public static final String COLUMN_ID_CATEGORY = "idCategory";
    }
}
