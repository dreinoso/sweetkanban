package com.chainreaction.sweetkanban.data;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.chainreaction.sweetkanban.R;

/**
 * Created by denis on 13/10/17.
 */

public class TasksPerCategoryAdapter extends CursorAdapter{

    private LayoutInflater mCursorInflater;
    private Context mContext;

    public TasksPerCategoryAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, flags);
        mCursorInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
        //mCursorInflater = LayoutInflater.from(context);
    }

    @Override
    public View newView(Context view, Cursor tasks, ViewGroup parent) {
        // R.layout.list_row is your xml layout for each row
        return mCursorInflater.inflate(R.layout.task_per_catergory_list, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor tasks) {
        String taskName = tasks.getString(tasks.getColumnIndex(KanbanContract.TaskListEntry.COLUMN_TASK_NAME));
        int taskColor = tasks.getInt(tasks.getColumnIndex(KanbanContract.TaskListEntry.COLUMN_TASK_COLOR));
        int taskID = tasks.getInt(tasks.getColumnIndex(KanbanContract.TaskListEntry._ID));

        //TextView taskText = new TextView(context);
        TextView taskText = (TextView) view.findViewById(R.id.taskForCategoryName);
        //taskText.setText(taskName); TODO uncomment
        if (taskText != null) {
            taskText.setText(taskName);
            taskText.setTextColor(taskColor);
            taskText.setId(taskID);
            notifyDataSetChanged();
        }
    }
}
