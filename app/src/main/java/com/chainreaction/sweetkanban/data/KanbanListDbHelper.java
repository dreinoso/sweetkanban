package com.chainreaction.sweetkanban.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.chainreaction.sweetkanban.data.KanbanContract.*;

public class KanbanListDbHelper extends SQLiteOpenHelper {

    private static KanbanListDbHelper sInstance;
    private static final String DATABASE_NAME = "kanbanlist.db";
    // If change the database schema, increment the database version
    private static final int DATABASE_VERSION = 4;

    private KanbanListDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // table to hold kanbalist data
        final String SQL_CREATE_KANBAN_TABLE = "CREATE TABLE " + KanbanListEntry.TABLE_NAME + " (" +
                KanbanListEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                KanbanListEntry.COLUMN_KANBAN_NAME + " TEXT NOT NULL, " +
                KanbanListEntry.COLUMN_KANBAN_COLOR + " TEXT NOT NULL, " +
                KanbanListEntry.COLUMN_KANBAN_POSITION + " TEXT NOT NULL," +
                KanbanListEntry.COLUMN_KANBAN_START_CATEGORY_ID + " INTEGER NOT NULL" +
                "); ";

        sqLiteDatabase.execSQL(SQL_CREATE_KANBAN_TABLE); // pass the string query
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // For now simply drop the table and create a new one. This means if you change the
        // DATABASE_VERSION the table will be dropped.
        // TODO In a production app, this method might be modified to ALTER the table
        // instead of dropping it, so that existing data is not deleted.
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + KanbanListEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public static synchronized KanbanListDbHelper getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        if (sInstance == null) {
            sInstance = new KanbanListDbHelper(context.getApplicationContext());
        }
        return sInstance;
    }


    public static String getDataBaseName() {
        return DATABASE_NAME;
    }
}