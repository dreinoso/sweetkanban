package com.chainreaction.sweetkanban.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.chainreaction.sweetkanban.data.KanbanContract.CategoryListEntry;

public class CategoryListDbHelper extends SQLiteOpenHelper {

    private static CategoryListDbHelper sInstance;
    private static final String DATABASE_NAME = "categorylist.db";
    // If change the database schema, increment the database version
    private static final int DATABASE_VERSION = 4;

    private CategoryListDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // table to hold kanbalist data
        final String SQL_CREATE_CATEGORY_TABLE = "CREATE TABLE " + CategoryListEntry.TABLE_NAME + " (" +
                CategoryListEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                CategoryListEntry.COLUMN_CATEGORY_NAME + " TEXT NOT NULL, " +
                CategoryListEntry.COLUMN_CATEGORY_COLOR + " TEXT NOT NULL, " +
                CategoryListEntry.COLUMN_CATEGORY_POSITION + " INTEGER NOT NULL," +
                CategoryListEntry.COLUMN_CATEGORY_TASK_COUNT + " INTEGER NOT NULL," +
                CategoryListEntry.COLUMN_CATEGORY_KANBAN_ID + " INTEGER NOT NULL" +
                "); ";

        sqLiteDatabase.execSQL(SQL_CREATE_CATEGORY_TABLE); // pass the string query
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // For now simply drop the table and create a new one. This means if you change the
        // DATABASE_VERSION the table will be dropped.
        // TODO In a production app, this method might be modified to ALTER the table
        // instead of dropping it, so that existing data is not deleted.
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CategoryListEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public static synchronized CategoryListDbHelper getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        if (sInstance == null) {
            sInstance = new CategoryListDbHelper(context.getApplicationContext());
        }
        return sInstance;
    }
}