package com.chainreaction.sweetkanban.ui;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.chainreaction.common.ConstantsDefined;
import com.chainreaction.common.ExtraUtils;
import com.chainreaction.sweetkanban.R;

import java.util.ArrayList;

public class AddKanbanActivity extends ListActivity {

    private final String TAG = this.getClass().getName();
    private final static boolean DEBUG = true;
    private static Context mContext;
    private ArrayList<String> mKanbanList;
    private ArrayAdapter<String> mKanbanListAdapter; // handle data of the list
    private int mKanbanCategoriesAdded = 0;

    private EditText mNewKanbanName;
    private EditText mNewKanbanCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_kanban);

        mContext = this.getBaseContext();

        mNewKanbanName = (EditText) findViewById(R.id.newKanbanNameEditText);
        mNewKanbanCategory = (EditText) findViewById(R.id.newKanbanCategoryEditText);
        mKanbanList = new ArrayList<String>();
        mKanbanListAdapter=new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                mKanbanList);
        setListAdapter(mKanbanListAdapter);
    }

    // Functions called by views

    /***
     * Handel dynamic insertions.
     * @param view
     */
    public void addKanbanCategory(View view) {
        if (mNewKanbanCategory.getText().length() == 0) {
            Toast.makeText(this, R.string.toast_error_empty_kanban_category, Toast.LENGTH_SHORT).show();
        } else {
            mKanbanList.add(mNewKanbanCategory.getText().toString());
            mKanbanListAdapter.notifyDataSetChanged();
            mNewKanbanCategory.clearFocus();
            mNewKanbanCategory.getText().clear();
        }
    }

    public void saveKanbanBtn(View view) {
        if (DEBUG) { Log.d(TAG, "saveKanbanBtn  mNewKanbanName = " + mNewKanbanName.getText().toString()); }
        if (mNewKanbanName.getText().length() == 0) {
            Toast.makeText(this, R.string.toast_error_empty_kanban_name, Toast.LENGTH_SHORT).show();
        } else if (false) { // TODO the check should be about if that nanme already exists
            Toast.makeText(this, R.string.toast_error_kanban_name_already_exist, Toast.LENGTH_SHORT).show();
        } else if (mKanbanList.isEmpty()) {
            Toast.makeText(this, R.string.toast_error_empty_categories, Toast.LENGTH_SHORT).show();
        } else { // it's a good kanban
            Intent resultIntent = new Intent();
            resultIntent.putExtra(ConstantsDefined.EXTRA_KANBAN_TYPE_NAME, mNewKanbanName.getText().toString());
            // TODO the color should not be hard coded
            resultIntent.putExtra(ConstantsDefined.EXTRA_KANBAN_TYPE_COLOR, R.color.colorPrimaryLight);
            resultIntent.putStringArrayListExtra(ConstantsDefined.EXTRA_KANBAN_CATEGORIES, mKanbanList);
            setResult(Activity.RESULT_OK, resultIntent);
            finish();
        }
    }

    public void discardKanbaBtn(View view) {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }
}
