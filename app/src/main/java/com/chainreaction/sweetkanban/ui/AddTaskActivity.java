package com.chainreaction.sweetkanban.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.chainreaction.common.ConstantsDefined;
import com.chainreaction.sweetkanban.R;

import java.util.Calendar;
import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class AddTaskActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getName();
    private final static boolean DEBUG = true;
    private static Context mContext;

    private EditText mNewTaskName;
    private EditText mDeadline;
    private EditText mAssigned;
    private Spinner mPrioritySpinner;
    private Spinner mColorSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        mContext = this.getBaseContext();

        mNewTaskName = (EditText) findViewById(R.id.newTaskNameEditText);
        mAssigned = (EditText) findViewById(R.id.taskAssignedToEditText);
        mDeadline = (EditText) findViewById(R.id.taskDeadlinEditText);

        mPrioritySpinner = (Spinner) findViewById(R.id.prioritySpinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.task_priority_levels, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mPrioritySpinner.setAdapter(adapter);

        mColorSpinner = (Spinner) findViewById(R.id.colorSpinner);
        ArrayAdapter<CharSequence> colorAdapter = ArrayAdapter.createFromResource(this,
                R.array.task_colors, android.R.layout.simple_spinner_item);
        colorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mColorSpinner.setAdapter(colorAdapter);
    }

    // Functions called by views
    public void saveTaskBtn(View view) {
        if (DEBUG) { Log.d(TAG, "saveTaskBtn  mNewTaskName = " + mNewTaskName.getText().toString()); }
        if (mNewTaskName.getText().length() == 0) {
            Toast.makeText(this, R.string.toast_error_empty_task_name, Toast.LENGTH_SHORT).show();
        } else if (false) { // TODO the check should be about if that name already exists
            Toast.makeText(this, R.string.toast_error_task_name_already_exist, Toast.LENGTH_SHORT).show();
        } else if (isValidDate(mDeadline.getText().toString())) {
            Intent resultIntent = new Intent();
            resultIntent.putExtra(ConstantsDefined.EXTRA_TASK_TYPE_NAME, mNewTaskName.getText().toString());
            resultIntent.putExtra(ConstantsDefined.EXTRA_TASK_TYPE_ASSIGNED, mAssigned.getText().toString());
            resultIntent.putExtra(ConstantsDefined.EXTRA_TASK_TYPE_DEADLINE, mDeadline.getText().toString());
            int color = ConstantsDefined.COLORS.get(mColorSpinner.getSelectedItem().toString());
            resultIntent.putExtra(ConstantsDefined.EXTRA_TASK_TYPE_COLOR, color);
            String priority = mPrioritySpinner.getSelectedItem().toString();
            resultIntent.putExtra(ConstantsDefined.EXTRA_TASK_TYPE_PRIORITY, priority); // TODO the priority should be a selected menu 1...7
            setResult(Activity.RESULT_OK, resultIntent);
            finish();
        } // Bad data provided by the user, nothing to do
    }

    public void discardTaskBtn(View view) {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    private boolean isValidDate(String inputDate) {
        Boolean result = true;
        if (inputDate == null || inputDate.isEmpty()) {
            mDeadline.setText("");
        } else { // There is a date to test
            try {
                DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                formatter.setLenient(false);
                Date deadLineDate = formatter.parse(inputDate);
                Calendar yesterday = Calendar.getInstance(); // this would default to now
                yesterday.add(Calendar.DAY_OF_MONTH, -1);

                if(deadLineDate.before(yesterday.getTime())) {
                    Toast.makeText(this, R.string.toast_error_bad_date_time, Toast.LENGTH_SHORT).show();
                    result = false;
                }
            } catch (ParseException e) {
                Toast.makeText(this, R.string.toast_error_bad_date_format, Toast.LENGTH_SHORT).show();
                result = false;
            }
        }

        return result;
    }
}
