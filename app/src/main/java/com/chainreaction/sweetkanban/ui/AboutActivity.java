package com.chainreaction.sweetkanban.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.chainreaction.sweetkanban.R;

/**
 * Created by denis on 15/05/17.
 */

public class AboutActivity extends Activity {

    private Button mLicenseBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        mLicenseBtn =  (Button) findViewById(com.chainreaction.sweetkanban.R.id.app_license);
        mLicenseBtn.setOnClickListener(mLicenseBtnClickListener);
    }

    private OnClickListener mLicenseBtnClickListener = new OnClickListener() {
        boolean dummyFlag = false;

        @Override
        public void onClick(View dialog) {
            if (dummyFlag) {
                Toast.makeText(getBaseContext(), "This should be deleted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getBaseContext(), "This should be deleted long", Toast.LENGTH_LONG).show();
            }
            dummyFlag = !dummyFlag;
        }
    };
}
