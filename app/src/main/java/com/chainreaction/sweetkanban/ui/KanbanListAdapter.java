package com.chainreaction.sweetkanban.ui;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chainreaction.sweetkanban.R;
import com.chainreaction.sweetkanban.data.KanbanContract;
import com.chainreaction.sweetkanban.ui.Interfaces.IKanbanShower;

public class KanbanListAdapter extends RecyclerView.Adapter<KanbanListAdapter.KanbanViewHolder> {

    private Context mContext;
    private Cursor mCursor; // Holds on to the cursor to display the kanbanList
    private IKanbanShower mKanbanShower; // Show the kanban selected from the recyclerview

    /**
     * Constructor using the context and the db cursor
     * @param context the calling context/activity
     * @param cursor the db cursor with waitlist data to display
     */
    public KanbanListAdapter(Context context, Cursor cursor, IKanbanShower kanbanShower) {
        this.mContext = context;
        this.mCursor = cursor;
        this.mKanbanShower = kanbanShower;
    }

    @Override
    public KanbanViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Get the RecyclerView item layout
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.list_item_kanban, parent, false);
        return new KanbanViewHolder(view);
    }

    @Override
    public void onBindViewHolder(KanbanViewHolder holder, int position) {
        if (mCursor.moveToPosition(position)) {
            String kanbanName = mCursor.getString(mCursor.getColumnIndex(KanbanContract.KanbanListEntry.COLUMN_KANBAN_NAME));
            String kanbanColor = mCursor.getString(mCursor.getColumnIndex(KanbanContract.KanbanListEntry.COLUMN_KANBAN_COLOR));
            int kanbanPosition = mCursor.getInt(mCursor.getColumnIndex(KanbanContract.KanbanListEntry.COLUMN_KANBAN_POSITION));
            holder.nameTextView.setText(kanbanName); // Display the guest name
            // TODO the list should set color and position for it's items
        }
    }

    @Override
    public int getItemCount() {
        return mCursor.getCount();
    }

    /**
     * Swaps the Cursor currently held in the adapter with a new one
     * and triggers a UI refresh
     *
     * @param newCursor the new cursor that will replace the existing one
     */
    public void swapCursor(Cursor newCursor) {
        if (mCursor != null) mCursor.close(); // Always close the previous mCursor first
        mCursor = newCursor;
        if (newCursor != null) {
            this.notifyDataSetChanged(); // Force the RecyclerView to refresh
        }
    }

    /**
     * Inner class to hold the views needed to display a single item in the recycler-view
     */
    class KanbanViewHolder extends RecyclerView.ViewHolder {
        // Will display the guest name
        TextView nameTextView;

        /**
         * Constructor for our ViewHolder. Within this constructor, we get a reference to our
         * TextViews
         *
         * @param itemView The View that you inflated in
         *                 {@link KanbanListAdapter#onCreateViewHolder(ViewGroup, int)}
         */
        public KanbanViewHolder(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.kanbanNameForListText);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String textFromView = nameTextView.getText().toString();
                    mKanbanShower.showKanban(v, textFromView);
                }
            });
        }

    }
}