package com.chainreaction.sweetkanban.ui;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.chainreaction.common.ConstantsDefined;
import com.chainreaction.sweetkanban.R;
import com.chainreaction.sweetkanban.data.KanbanContract;
import com.chainreaction.sweetkanban.data.KanbanListDbHelper;
import com.chainreaction.sweetkanban.data.CategoryListDbHelper;
import com.chainreaction.sweetkanban.ui.Interfaces.IKanbanShower;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements IKanbanShower{

    private final int DEFAULT_TASK_COUNT = 0;
    private final int DEFAULT_START_CATEGORY_ID = 0;
    private final String TAG = this.getClass().getSimpleName();
    private final static boolean DEBUG = true;
    private static final int ADD_KANBAN_REQUEST = 1;

    private KanbanListAdapter mKanbanListAdapter;
    private SQLiteDatabase mKanbanDb;
    private SQLiteDatabase mCategoryDb;

    private static int mLastKanbanPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.appBarLayout);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.addKanbanButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(MainActivity.this, AddKanbanActivity.class), ADD_KANBAN_REQUEST);
            }
        });

        RecyclerView kanbanListRecyclerView;
        kanbanListRecyclerView = (RecyclerView) this.findViewById(R.id.all_kanban_list_view);
        kanbanListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mKanbanDb = KanbanListDbHelper.getInstance(this).getWritableDatabase(); // Keep a reference to mKanbanDb until paused or killed
        Cursor kanbanCursor = getAllKanbans();
        mKanbanListAdapter = new KanbanListAdapter(this, kanbanCursor, this); // to display the data
        kanbanListRecyclerView.setAdapter(mKanbanListAdapter); // Link the adapter to the RecyclerView
        mLastKanbanPosition = kanbanCursor.getCount();

        mCategoryDb = CategoryListDbHelper.getInstance(this).getWritableDatabase();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(com.chainreaction.sweetkanban.R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
        } else if (id == R.id.action_about) {
            startActivity(new Intent(MainActivity.this, AboutActivity.class));
        } else if (id == R.id.action_exit) {
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check which is the current result
        if (requestCode == ADD_KANBAN_REQUEST) {
            if (resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();
                if (extras != null) {
                    String name = extras.getString(ConstantsDefined.EXTRA_KANBAN_TYPE_NAME);
                    int color = extras.getInt(ConstantsDefined.EXTRA_KANBAN_TYPE_COLOR);
                    long kanbanID = addToKanbanList(name, color);
                    long startCategoryID = addCategories(extras.getStringArrayList(
                            ConstantsDefined.EXTRA_KANBAN_CATEGORIES), kanbanID);
                    updateStartCategory(kanbanID, startCategoryID);
                }
            } else if (resultCode == RESULT_CANCELED) {
                if (DEBUG) {Log.d(TAG, "onActivityResult result code for ADD_KANBAN_REQUEST: RESULT_CANCELED");}
            } else {
                Log.e(TAG, "onActivityResult bad result code for ADD_KANBAN_REQUEST");
            }
        }
    }

    private void updateStartCategory(long kanbanId, long startCategoryId) {
        ContentValues cv = new ContentValues(); // to pass the values onto the update query
        cv.put(KanbanContract.KanbanListEntry.COLUMN_KANBAN_START_CATEGORY_ID, startCategoryId);
        mKanbanDb.update(KanbanContract.KanbanListEntry.TABLE_NAME, cv, "_id="+kanbanId, null);
        mKanbanListAdapter.swapCursor(getAllKanbans());
    }

    /**
     * Query the mKanbanDb and get all kanbans from the waitlist table
     *
     * @return Cursor containing the list of guests
     */
    private Cursor getAllKanbans() {
        return mKanbanDb.query(
                KanbanContract.KanbanListEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                KanbanContract.KanbanListEntry.COLUMN_KANBAN_POSITION
        );
    }

    /**
     * Adds a new Kanban top the data base.
     * @param name Name of the new kanban.
     * @param color Color of the new kanban.
     * @return id of new record added
     */
    private long addNewKanban(String name, int color) {
        ContentValues cv = new ContentValues(); // to pass the values onto the insert query
        cv.put(KanbanContract.KanbanListEntry.COLUMN_KANBAN_NAME, name);
        cv.put(KanbanContract.KanbanListEntry.COLUMN_KANBAN_COLOR, color);
        mLastKanbanPosition++; // Always add a new kanban to the end, then could be changed
        cv.put(KanbanContract.KanbanListEntry.COLUMN_KANBAN_POSITION, mLastKanbanPosition);
        cv.put(KanbanContract.KanbanListEntry.COLUMN_KANBAN_START_CATEGORY_ID, DEFAULT_START_CATEGORY_ID);
        return mKanbanDb.insert(KanbanContract.KanbanListEntry.TABLE_NAME, null, cv);
    }

    /**
     * Adds a new set of categories for certain kanban.
     * @param categories The list of categories to add to the data base.
     * @param kanbanId ID for the kanban, which this categories belong.
     * @return ID of the start category.
     */
    private long addCategories(ArrayList<String> categories, long kanbanId) {
        ContentValues cv = new ContentValues(); // to pass the values onto the insert query
        int categoryPosition = 0;
        long firstRecordId = -1;
        boolean isFirstRecord = true;

        for (String category : categories) {
            cv.put(KanbanContract.CategoryListEntry.COLUMN_CATEGORY_NAME, category);
            //TODO this color should not be hard coded
            cv.put(KanbanContract.CategoryListEntry.COLUMN_CATEGORY_COLOR,  R.color.colorPrimaryDark);
            cv.put(KanbanContract.CategoryListEntry.COLUMN_CATEGORY_POSITION, categoryPosition);
            categoryPosition++;
            cv.put(KanbanContract.CategoryListEntry.COLUMN_CATEGORY_TASK_COUNT, (int) DEFAULT_TASK_COUNT);
            cv.put(KanbanContract.CategoryListEntry.COLUMN_CATEGORY_KANBAN_ID, (int) kanbanId);

            if (isFirstRecord) {
                firstRecordId = mCategoryDb.insert(KanbanContract.CategoryListEntry.TABLE_NAME, null, cv);
                isFirstRecord = false;
            } else { // No need to keep other IDs just the first one
                mCategoryDb.insert(KanbanContract.CategoryListEntry.TABLE_NAME, null, cv);
            }
        }

        if (isFirstRecord) {
            throw new IllegalStateException("No categories added for Kanban with id: " + kanbanId);
        } // else: it's fine we can return the startCategoryID

        return firstRecordId;
    }

    /**
     * Adds new kanban element to the list.
     * @param name Name of the new kanban.
     * @param color Color of the new kanban.
     */
    public long addToKanbanList(String name, int color) {
        long newKanbanId = addNewKanban(name, color);
        mKanbanListAdapter.swapCursor(getAllKanbans()); // Update the cursor to display the new list
        return newKanbanId;
    }

    public void showKanban(View v, String kanbanName) {
        Intent showKanbanIntent = new Intent(MainActivity.this, KanbanActivity.class);
        showKanbanIntent.putExtra(ConstantsDefined.EXTRA_SHOW_KANBAN_TYPE_KANBAN_NAME, kanbanName);

        Cursor selectedKanbanCursor = getSelectedKanbanCursor(kanbanName);
        selectedKanbanCursor.moveToFirst();
        String kanbanId = selectedKanbanCursor.getString(selectedKanbanCursor.
                getColumnIndex(KanbanContract.KanbanListEntry._ID));
        showKanbanIntent.putExtra(ConstantsDefined.EXTRA_SHOW_KANBAN_TYPE_KANBAN_ID, kanbanId);

        String startCategoryId = selectedKanbanCursor.getString(selectedKanbanCursor.
                getColumnIndex(KanbanContract.KanbanListEntry.COLUMN_KANBAN_START_CATEGORY_ID));
        showKanbanIntent.putExtra(ConstantsDefined.EXTRA_SHOW_KANBAN_TYPE_START_CATEGORY_ID, startCategoryId);

        if (DEBUG) { Log.d(TAG, "showKanban, kanban name: " + kanbanName + "; kanban id: " + kanbanId); }
        startActivity(showKanbanIntent);
    }

    /***
     * Gets the cursor for the category with that name.
     * @param kanbanName Name of the kanban.
     * @return The cursor with all the data of that category.
     */
    private Cursor getSelectedKanbanCursor(String kanbanName) {
        return mKanbanDb.query(
                KanbanContract.KanbanListEntry.TABLE_NAME,
                null,
                KanbanContract.KanbanListEntry.COLUMN_KANBAN_NAME + " ='" + kanbanName + "'",
                null,
                null,
                null,
                KanbanContract.KanbanListEntry.COLUMN_KANBAN_POSITION
        );
    }
}
