package com.chainreaction.sweetkanban.ui.Interfaces;

import android.view.View;

public interface IKanbanShower {

    /**
     * Shows the kanban selected
     * @param v Element selected to be shown
     */
    void showKanban(View view, String textFromView);
}
