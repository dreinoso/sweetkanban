package com.chainreaction.sweetkanban.ui;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.GridLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.chainreaction.common.ConstantsDefined;
import com.chainreaction.sweetkanban.R;
import com.chainreaction.sweetkanban.data.CategoryListDbHelper;
import com.chainreaction.sweetkanban.data.KanbanContract;
import com.chainreaction.sweetkanban.data.TaskListDbHelper;
import com.chainreaction.sweetkanban.data.TasksPerCategoryAdapter;

import java.util.ArrayList;

public class KanbanActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getSimpleName();
    private final boolean DEBUG = true;
    private final int ADD_TASK_REQUEST = 1;

    private SQLiteDatabase mCategoryDb;
    private SQLiteDatabase mTaskDb;

    private String mKanbanName;
    private String mKanbanID;
    private String mStartCategoryID; // Used when adding a new task

    private Button mNewTaskBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kanban);

        mNewTaskBtn = (Button) findViewById(R.id.newTaskBtn);
        mCategoryDb = CategoryListDbHelper.getInstance(this).getReadableDatabase(); // Keep a reference to mTaskDb until paused or killed
        mTaskDb = TaskListDbHelper.getInstance(this).getWritableDatabase(); // Keep a reference to mTaskDb until paused or killed

        initUI();
    }

    private void initUI() {
        mNewTaskBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(KanbanActivity.this, AddTaskActivity.class), ADD_TASK_REQUEST);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mKanbanName = getIntent().getExtras().getString(ConstantsDefined.EXTRA_SHOW_KANBAN_TYPE_KANBAN_NAME);
        mKanbanID = getIntent().getExtras().getString(ConstantsDefined.EXTRA_SHOW_KANBAN_TYPE_KANBAN_ID);
        mStartCategoryID = getIntent().getExtras().getString(ConstantsDefined.EXTRA_SHOW_KANBAN_TYPE_START_CATEGORY_ID);

        initGrid();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which is the current result
        if (requestCode == ADD_TASK_REQUEST) {
            if (resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();
                if (extras != null) {
                    String name = extras.getString(ConstantsDefined.EXTRA_TASK_TYPE_NAME);
                    String assignedTo = extras.getString(ConstantsDefined.EXTRA_TASK_TYPE_ASSIGNED);
                    String deadline = extras.getString(ConstantsDefined.EXTRA_TASK_TYPE_DEADLINE);
                    String priority = extras.getString(ConstantsDefined.EXTRA_TASK_TYPE_PRIORITY);
                    int color = extras.getInt(ConstantsDefined.EXTRA_TASK_TYPE_COLOR);

                    addToTaskList(name, assignedTo, deadline, priority, color);
                }
            } else if (resultCode == RESULT_CANCELED) {
                if (DEBUG) Log.d(TAG, "onActivityResult result code for ADD_TASK_REQUEST: RESULT_CANCELED");
            } else {
                Log.e(TAG, "onActivityResult bad result code for ADD_TASK_REQUEST");
            }
        }
    }

    public void initGrid() {
        Cursor categories = getCurrentCategories();
        GridLayout kanbanGrid = (GridLayout) findViewById(R.id.kanbanGrid);
        kanbanGrid.removeAllViews(); // TODO find a better way of updating just the view that have changed
        kanbanGrid.setAlignmentMode(GridLayout.ALIGN_BOUNDS);
        kanbanGrid.setColumnCount(categories.getCount());
        kanbanGrid.setUseDefaultMargins(true);
        kanbanGrid.setMinimumWidth(200);
        kanbanGrid.setMinimumHeight(200);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            kanbanGrid.setForegroundGravity(Gravity.CENTER);
        }

        kanbanGrid.setOverScrollMode(View.OVER_SCROLL_ALWAYS);

        try {
            ArrayList<TextView> categoryViews = new ArrayList<>();
            ArrayList<ListView> taskListViews = new ArrayList<>();

            // Since the cursor start at the 0 position, no need to call moveFirst
            while (categories.moveToNext()) {
                fillViewLists(categories, categoryViews, taskListViews);
            }

            int gridIndex = 0;

            for(TextView categoryElement : categoryViews) {
                // Take care about adding param settings as width could make views overlap each other
                kanbanGrid.addView(categoryElement, gridIndex);
                gridIndex++;
            }

            for(ListView taskListElement : taskListViews) {
                if (taskListElement != null) {
                    if (taskListElement.getParent() != null) {
                        ((ViewGroup) taskListElement.getParent()).removeView(taskListElement);
                    } // else there is no parent

                    kanbanGrid.addView(taskListElement, gridIndex);
                    gridIndex++;
                }
            }
        } finally {
            // todo: should be closed here the cursors?
            //categories.close();
            //if (tasks != null) { tasks.close(); }
        }
    }

    private void fillViewLists(Cursor categories, ArrayList<TextView> categoryViews,
                                            ArrayList<ListView> taskListViews) {
        int categoryID = categories.getInt(categories.getColumnIndex(KanbanContract.CategoryListEntry._ID));
        String categoryName = categories.getString(categories.getColumnIndex(KanbanContract.CategoryListEntry.COLUMN_CATEGORY_NAME));
        int categoryColor = categories.getInt(categories.getColumnIndex(KanbanContract.CategoryListEntry.COLUMN_CATEGORY_COLOR));
        int taskCount = categories.getInt(categories.getColumnIndex(KanbanContract.CategoryListEntry.COLUMN_CATEGORY_TASK_COUNT));

        // First save all categories to be add later, since we have not already all the categories
        // and need to be add in order
        TextView categoryText = new TextView(this);
        categoryText.setText(categoryName);
        categoryText.setTextColor(categoryColor);
        categoryText.setId(categoryID);
        categoryViews.add(categoryText);
        Cursor tasks = getAllTaskForCategory(categoryID);
        fillTaskList(tasks, taskListViews, categoryID);
    }

    private void fillTaskList(Cursor tasks, ArrayList<ListView> taskListViews,int categoryID) {
        // todo: task list is not shown at the first category
        ListView taskList = new ListView(this);
        taskList.setId(categoryID);

        if (tasks.moveToNext()) {
            CursorAdapter taskPerCategoryAdapter = new TasksPerCategoryAdapter(this, tasks, 0);
            taskList.setAdapter(taskPerCategoryAdapter);
        } // else: there is no tasks for this category to be loaded

        //taskPerCategoryAdapter.notifyDataSetChanged();
        // These lists must be loaded after the categories, so are saved by now
        taskListViews.add(taskList);
    }

    private long addToTaskList(String name, String assignedTo, String deadline, String priority, int color) {
        ContentValues cv = new ContentValues(); // to pass the values onto the insert query
        cv.put(KanbanContract.TaskListEntry.COLUMN_TASK_NAME, name);
        cv.put(KanbanContract.TaskListEntry.COLUMN_TASK_ASSIGN_TO, assignedTo);
        cv.put(KanbanContract.TaskListEntry.COLUMN_TASK_DEADLINE, deadline);
        cv.put(KanbanContract.TaskListEntry.COLUMN_TASK_POSITION, getCurrentTaskPosition());
        cv.put(KanbanContract.TaskListEntry.COLUMN_TASK_PRIORITY, priority);
        cv.put(KanbanContract.TaskListEntry.COLUMN_TASK_COLOR, color);
        cv.put(KanbanContract.TaskListEntry.COLUMN_ID_CATEGORY, mStartCategoryID);
        return mTaskDb.insert(KanbanContract.TaskListEntry.TABLE_NAME, null, cv);
    }

    public Cursor getCurrentCategories() {
        Cursor cursor = mCategoryDb.query(
                KanbanContract.CategoryListEntry.TABLE_NAME,
                null,
                KanbanContract.CategoryListEntry.COLUMN_CATEGORY_KANBAN_ID + " = " + mKanbanID,
                null,
                null,
                null,
                KanbanContract.CategoryListEntry.COLUMN_CATEGORY_POSITION
        );
        return cursor;
    }

    public Cursor getAllTaskForCategory (int categoryID) {
        Cursor cursor = mTaskDb.query(
                KanbanContract.TaskListEntry.TABLE_NAME,
                null,
                KanbanContract.TaskListEntry.COLUMN_ID_CATEGORY + " = " + categoryID,
                null,
                null,
                null,
                KanbanContract.TaskListEntry.COLUMN_TASK_POSITION
        );
        return cursor;
    }

    /***
     * Gets the current position of the new task, like the new task goes at the end, then must calculate
     * the elements that are already on the table.
     *
     * @return Count of elements on the start category column.
     */
    private int getCurrentTaskPosition() {
        Cursor cursor = mTaskDb.query(
                KanbanContract.TaskListEntry.TABLE_NAME,
                null,
                KanbanContract.TaskListEntry.COLUMN_ID_CATEGORY + " = " + mStartCategoryID,
                null,
                null,
                null,
                KanbanContract.TaskListEntry.COLUMN_TASK_POSITION
        );
        return cursor.getCount(); // TODO in general check if the cursors should be closed
    }

    /**
     * Query the mTaskDb and get all tasks from the waitlist table
     *
     * @return Cursor containing the list of guests
     */
    private Cursor getAllTasks() {
        return mTaskDb.query(
                KanbanContract.TaskListEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                KanbanContract.TaskListEntry.COLUMN_TASK_POSITION
        );
    }

    private void addTaskToTable(Cursor categories, Cursor tasks, ListView taskList) {
        String taskName = tasks.getString(tasks.getColumnIndex(KanbanContract.TaskListEntry.COLUMN_TASK_NAME));
        int taskColor = tasks.getInt(tasks.getColumnIndex(KanbanContract.TaskListEntry.COLUMN_TASK_COLOR));

        TextView taskText = new TextView(this);
        taskText.setText(taskName);
        taskText.setTextColor(taskColor);
        taskList.addView(taskText);
    }
}
