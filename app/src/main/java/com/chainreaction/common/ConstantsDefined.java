package com.chainreaction.common;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;

import com.chainreaction.sweetkanban.R;

import java.util.HashMap;

/**
 * Class to keep constants of the Kanban application.
 */
public class ConstantsDefined {

    // Extra types for adding kanbans used by on result
    public final static String EXTRA_KANBAN_TYPE_NAME = "EXTRA_KANBAN_TYPE_NAME";
    public final static String EXTRA_KANBAN_TYPE_COLOR = "EXTRA_KANBAN_TYPE_COLOR";
    public static final String EXTRA_KANBAN_CATEGORIES = "EXTRA_KANBAN_CATEGORIES";

    // Extra types for adding tasks used by on result
    public final static String EXTRA_TASK_TYPE_NAME = "EXTRA_TASK_TYPE_NAME";
    public final static String EXTRA_TASK_TYPE_COLOR = "EXTRA_TASK_TYPE_COLOR";
    public final static String EXTRA_TASK_TYPE_DEADLINE = "EXTRA_TASK_TYPE_DEADLINE";
    public final static String EXTRA_TASK_TYPE_PRIORITY = "EXTRA_TASK_TYPE_PRIORITY";
    public final static String EXTRA_TASK_TYPE_ASSIGNED = "EXTRA_TASK_TYPE_ASSIGNED";

    // Extra type to get the selected kanban
    public final static String EXTRA_SHOW_KANBAN_TYPE_KANBAN_NAME = "EXTRA_SHOW_KANBAN_TYPE_KANBAN_NAME";
    public final static String EXTRA_SHOW_KANBAN_TYPE_KANBAN_ID = "EXTRA_SHOW_KANBAN_TYPE_KANBAN_ID";
    public final static String EXTRA_SHOW_KANBAN_TYPE_START_CATEGORY_ID = "EXTRA_SHOW_KANBAN_TYPE_START_CATEGORY_ID";

    // Map to store the colors used by the system
    public final static HashMap<String,Integer> COLORS = new HashMap<String,Integer>() {
        {
            put("White", Integer.valueOf(R.color.white));
            put("Yellow", Integer.valueOf(R.color.yellow));
            put("Green", Integer.valueOf(R.color.green));
            put("Cyan", Integer.valueOf(R.color.cyan));
            put("Blue", Integer.valueOf(R.color.blue));
            put("Orange", Integer.valueOf(R.color.orange));
            put("Purple", Integer.valueOf(R.color.purple));
            put("Red", Integer.valueOf(R.color.red));
            put("Black", Integer.valueOf(R.color.black));
        }
    };
}
