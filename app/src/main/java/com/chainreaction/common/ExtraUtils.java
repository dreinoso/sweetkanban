package com.chainreaction.common;

/**
 * Created by denis on 03/06/17.
 */

public class ExtraUtils {

    public static String getMethodName (Object targetClass) {
        return targetClass.getClass().getEnclosingMethod().getName();
    }
}
